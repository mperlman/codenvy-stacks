FROM eclipse/stack-base:ubuntu

EXPOSE 4403 8080 8000 22

USER root
RUN sudo apt-get update \
 && sudo apt-get install bzip2 -y \
 && sudo apt-get clean \
 && sudo apt-get -y autoremove \
 && sudo rm -rf /var/lib/apt/lists/*

USER user
WORKDIR /nix
RUN sudo chown user:user /nix \
 && curl https://nixos.org/nix/install | USER=user sh \
 && echo . /home/user/.nix-profile/etc/profile.d/nix.sh >> ~/.profile \
 && chmod 755 ~/.profile \
 && . ~/.profile \
 && nix-env -iA nixpkgs.gitMinimal \
 && nix-env -f "<nixpkgs>" -iA haskellPackages.stack

WORKDIR /projects
RUN sudo chown user:user /projects